const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../server")
const jwt = require("jsonwebtoken")
const assert = require("assert")
const pool = require("../src/dao/database")
const logger = require('tracer').console()
chai.should()
chai.use(chaiHttp)
var expect = chai.expect;


// UC-401 Aanmelden voor maaltijd
// TC-401-1 Niet ingelogd
// Geen aanmelding toegevoegdResponsestatus HTTP code 401
// Response bevat JSON object met daarin generieke foutinformatie.

describe("Participants", function () {
    it("TC-401-1 should return a 401 when not signed in", (done) => {
        chai.request(server)
            .post("/api/studenthome/1/meal/1/signup")
            .end((err, res) => {
                res.should.have.status(401)
                res.body.should.contain({
                    error: 'Authorization header missing!',
                });
                done()
            })
    })

    // TC-401-2 Maaltijd bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-401-2 should return a 404 when meal not found.", (done) => {
        chai.request(server)
            .post("/api/studenthome/1/meal/666/signup")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.contain({
                    Message: 'Invalid MealId or HomeId',
                });
                done()
            })
    })


    // TC-401-3 Succesvol aangemeld
    // Aanmelding is toegevoegdResponsestatus HTTP code 200 (OK)
    // Response bevat JSON object metalle gegevens van de aanmelding.

    it("TC-401-3 should return a 200 when succesfull.", (done) => {
        chai.request(server)
            .post("/api/studenthome/1/meal/2/signup")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.contain({
                    Message: 'You signed up to the meal!',
                });
                done()
            })
    })

    // UC-402 Afmelden voor maaltijd
    // TC-402-1 Niet ingelogd
    // Geen aanmelding toegevoegdResponsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-402-1 should return a 401 when not signed in", (done) => {
        chai.request(server)
            .put("/api/studenthome/1/meal/1/signoff")
            .end((err, res) => {
                res.should.have.status(401)
                res.body.should.contain({
                    error: 'Authorization header missing!',
                });
                done()
            })
    })


    // TC-402-2 Maaltijd bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-402-2 should return a 404 when meal not found.", (done) => {
        chai.request(server)
            .put("/api/studenthome/1/meal/666/signoff")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.contain({
                    Message: 'Meal of registration niet gevonden, vul een geldige ID in.',
                });
                done()
            })
    })


    // TC-402-3 Aanmelding bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-402-3 should return a 404 when registration not found.", (done) => {
        chai.request(server)
            .put("/api/studenthome/1/meal/666/signoff")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.contain({
                    Message: 'Meal of registration niet gevonden, vul een geldige ID in.',
                });
                done()
            })
    })


    // TC-403-4 Succesvol afgemeld
    // Aanmelding is verwijderd
    // Responsestatus HTTP code 200 (OK)

    it("TC-403-4 should return a 200 when sign off succesfull.", (done) => {
        chai.request(server)
            .put("/api/studenthome/1/meal/2/signoff")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.contain({
                    Message: 'You signed off to the meal!',
                });
                done()
            })
    })


    // UC-403 Lijst van deelnemers opvragen
    // TC-403-1 Niet ingelogd
    // Responsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-403-1 should return a 401 when not signed in", (done) => {
        chai.request(server)
            .get("/api/meal/1/participants")
            .end((err, res) => {
                res.should.have.status(401)
                res.body.should.contain({
                    error: 'Authorization header missing!',
                });
                done()
            })
    })



    // TC-403-2 Maaltijd bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-403-2 should return a 404 when not found", (done) => {
        chai.request(server)
            .get("/api/meal/666/participants")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.contain({
                    Message: 'No participants found',
                });
                done()
            })
    })


    // TC-403-3 Lijst van deelnemers geretourneerd
    // Responsestatus HTTP code 200 (OK)
    // Response bevat JSON object met contactgegevens van alle aangemelde gebruikers.
    // Response bevat niet het wachtwoord van de deelnemers.

    it("TC-403-3 should return a 200 when participants found", (done) => {
        chai.request(server)
            .get("/api/meal/1/participants")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .end((err, res) => {
                res.should.have.status(200)
                expect(res.body).to.be.an.instanceof(Array);
                expect(res.body).to.have.lengthOf.above(0);
                expect(res.body[0]).to.have.property('UserID');
                expect(res.body[0]).to.have.property('StudenthomeID');
                expect(res.body[0]).to.have.property('Student_Number');
                expect(res.body[0]).to.have.property('First_Name');
                expect(res.body[0]).to.have.property('StudenthomeID');
                expect(res.body[0]).to.have.property('SignedUpOn');
                done()
            })
    })


    // UC-404 Details van deelnemer opvragen
    // TC-404-1 Niet ingelogd
    // Responsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-404-1 should return 401 when not signed in", (done) => {
        chai.request(server)
            .get("/api/meal/1/participants/1")
            .end((err, res) => {
                res.should.have.status(401)
                res.body.should.contain({
                    error: 'Authorization header missing!',
                });
                done()
            })
    })

    // TC-404-2 Deelnemer bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-404-1 should return 401 when not signed in", (done) => {
        chai.request(server)
            .get("/api/meal/1/participants/666")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .end((err, res) => {
                res.should.have.status(404)
                res.body.should.contain({
                    Message: 'No participant found',
                });
                done()
            })
    })

    // TC-404-3 Contactgegevens van deelnemer geretourneerd
    // Responsestatus HTTP code 200 (OK)
    // Response bevat JSON object metcontactgegevens van gegeven deelnemer.
    // Response bevat niet het wachtwoord van de deelnemer.

    it("TC-404-3 should return 200 when participant found", (done) => {
        chai.request(server)
            .get("/api/meal/1/participants/1")
            .set("authorization", "Bearer " + jwt.sign({ id: 1 }, "secret"))
            .end((err, res) => {
                res.should.have.status(200)
                expect(res.body).to.be.an.instanceof(Array);
                expect(res.body).to.have.lengthOf.above(0);
                expect(res.body[0]).to.have.property('UserID');
                expect(res.body[0]).to.have.property('StudenthomeID');
                expect(res.body[0]).to.have.property('Student_Number');
                expect(res.body[0]).to.have.property('First_Name');
                expect(res.body[0]).to.have.property('StudenthomeID');
                expect(res.body[0]).to.have.property('SignedUpOn');
                done()
            })
    })
})

