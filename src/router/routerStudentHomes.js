var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');

const controllerStudentHomes = require("../controller/controllerStudentHomes");
const validation = require("../validation/validationStudentHomes");
const authcontroller = require("../controller/authenticationController");
const logger = require('tracer').console();

router.get('/info',
    controllerStudentHomes.getInfoFunction
);

router.get('/studenthomes',
    controllerStudentHomes.getStudentHomes
);

router.post('/studenthomes',
    authcontroller.validateToken,
    validation.validatePostalCodeAndCity,
    controllerStudentHomes.postNewStudentHomes
);

router.get('/studenthome/:homeId',
    controllerStudentHomes.getStudentHomeByID
);

router.put('/studenthomes/:homeId',
    authcontroller.validateToken,
    validation.validatePostalCodeAndCity,
    controllerStudentHomes.updateStudentHome
);

router.delete('/studenthomes/:homeId',
    authcontroller.validateToken,
    controllerStudentHomes.deleteStudentHome
);

module.exports = router;