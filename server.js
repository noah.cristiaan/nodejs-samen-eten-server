const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const authroutes = require("./src/router/authenticationRoutes"); 
const routerStudentHomes = require("./src/router/routerStudentHomes");
const routerParticipants = require("./src/router/routerParticipants");
const routerMeals = require("./src/router/routerMeals");
const logger = require('tracer').console();

app.use(express.json());

app.use("/api", routerStudentHomes);

app.use("/api", routerParticipants);

app.use("/api", routerMeals);

app.use('/api', authroutes);


app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader(
    'Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE'
  );
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-Requested-With,content-type,authorization'
  );
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
})

app.all('*', (req, res, next) => {
  const method = req.method;
  logger.debug('Method: ', method);
  next();
});

app.all('*', (req, res, next) => {
  res.status(404).json({
    error: 'Endpoint does not exist!'
  });
})

app.listen(port, () => {
  console.log(`Exampe app listening at http://localhost:${port}/`);
});

module.exports = app;





