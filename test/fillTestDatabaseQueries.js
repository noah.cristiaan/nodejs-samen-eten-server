const INSERT_MEAL = "INSERT INTO `meal` (`ID`, `Name`, `Description`, `Ingredients`, `Allergies`, `CreatedOn`, `OfferedOn`, `Price`, `UserID`, `StudenthomeID`, `MaxParticipants`) VALUES" +
"(1, 'Zuurkool met worst', 'Zuurkool a la Montizaan, specialiteit van het huis.', 'Zuurkool, worst, spekjes', 'Lactose, gluten', '2020-01-01 10:10:00', '2020-01-01 10:10:00', 5.5, 1, 1, 4)," +
"(2, 'Spaghetti', 'Spaghetti Bolognese', 'Pasta, tomatensaus, gehakt', 'Lactose', '2020-01-01 10:10:00', '2020-01-01 10:10:00', 3.25, 1, 1, 6)," +
"(3, 'AVG', 'lekker aardappel vlees en groente', 'aardappel,vlees,groente', 'boter,kaas,eieren', '2021-05-19 00:00:00', '2021-05-19 00:00:00', 24, 1, 1, 8)," +
"(4, 'AVG', 'lekker aardappel vlees en groente', 'aardappel,vlees,groente', 'boter,kaas,eieren', '2021-05-19 00:00:00', '2021-05-19 00:00:00', 24, 1, 1, 8);" 

const INSERT_STUDENTHOME = "INSERT INTO `studenthome` (`ID`, `Name`, `Address`, `House_Nr`, `UserID`, `Postal_Code`, `Telephone`, `City`) VALUES" +
"(1, 'leukeclownsschool', 'clownscshool', 12, 1, '2316KS', '0638605995', 'twente')," + 
"(2, 'Haagdijk 23', 'Haagdijk', 4, 4, '4706RX', '061234567891', 'Breda')," + 
"(3, 'Den Hout', 'Lovensdijkstraat', 61, 3, '4706RX', '061234567891', 'Den Hout')," +
"(4, 'Den Dijk', 'Langendijk', 63, 4, '4706RX', '061234567891', 'Breda')," +
"(5, 'Lovensdijk', 'Lovensdijkstraat', 62, 2, '4706RX', '061234567891', 'Breda')," +
"(6, 'Van Schravensteijn', 'Schravensteijnseweg', 23, 3, '4706RX', '061234567891', 'Breda')," +
"(10, 'TestenOsso', 'Princenhagen', 141, 1, '4225VW', '0638605995', 'Zunderd');"

const INSERT_USER = "INSERT INTO `user` (`ID`, `First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES" +
"(1, 'Jan', 'Smit', 'jsmit@server.nl', '222222', 'secret')," +
"(2, 'Mark', 'Gerrits', 'mark@gerrits.nl', '333333', 'secret')," +
"(3, 'Dion', 'Jansen', 'dion@jansen.nl', '444444', 'secret')," +
"(4, 'Marieke', 'van Dam', 'mariekevandam@home.nl', '555555', 'secretsecret');"

const INSERT_PARTICIPANTS = "INSERT INTO `participants` (`UserID`, `StudenthomeID`, `MealID`, `SignedUpOn`) VALUES" +
"(1, 1, 1, '2021-05-19')," +
"(2, 1, 1, '2021-05-19')," + 
"(3, 1, 1, '2021-05-19')," +
"(3, 1, 2, '2021-05-19')," + 
"(4, 1, 1, '2021-05-19')," +
"(4, 1, 2, '2021-05-19');"

const CLEAR_DB =
	"DELETE IGNORE FROM `studenthome`;" +
	"DELETE IGNORE FROM `user`;" +
	"DELETE IGNORE FROM `meal`;"

const CLEAR_STUDENTHOME_TABLE = "DELETE IGNORE FROM `studenthome`;"

const CLEAR_USERS_TABLE = "DELETE FROM `user`;"

module.exports = {INSERT_MEAL,INSERT_STUDENTHOME,INSERT_USER,INSERT_PARTICIPANTS,CLEAR_DB,CLEAR_STUDENTHOME_TABLE,CLEAR_USERS_TABLE}