var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
const validationParticipants = require("../validation/validationParticipants");
const controllerParticipants = require("../controller/controllerParticipants");
const authcontroller = require("../controller/authenticationController");

// UC-401 Aanmelden voor maaltijd
router.post('/studenthome/:homeId/meal/:mealId/signup',
    authcontroller.validateToken,
    validationParticipants.validateSignup,
    controllerParticipants.signupForMeal
);

// UC-402 Afmelden voor maaltijd
router.put('/studenthome/:homeId/meal/:mealId/signoff',
    authcontroller.validateToken,
    controllerParticipants.signoffForMeal
);

// UC-403 Lijst van deelnemers opvragen
router.get('/meal/:mealId/participants',
    authcontroller.validateToken,
    controllerParticipants.getParticipants
);

// UC-404 Details van deelnemer opvragen
router.get('/meal/:mealId/participants/:participantId',
    authcontroller.validateToken,
    controllerParticipants.getDetailsParticipants
);

module.exports = router;