let timeToWait = 1500;
const { json } = require("body-parser");
const database = require("../dao/database");
const { use } = require("../router/routerStudentHomes");
const { logger } = require("./databaseConfig");

module.exports = {

    // UC-201 Maak studentenhuis
    postStudentHome(req, res, next) {
        const naam = req.body.Name;
        const straatnaam = req.body.Address;
        const huisnummer = req.body.House_Nr;
        const postcode = req.body.Postal_Code;
        const plaats = req.body.City;
        const telefoonnummer = req.body.Telephone;
        const userID = req.userId;

        if (Object.keys(req.query).length === 0) {
            const query = {
                sql: "INSERT INTO studenthome (Address, City, House_Nr, Name, Postal_Code, Telephone, UserID) VALUES (?, ?, ?, ?, ?, ?, ?)",
                values: [straatnaam, plaats, huisnummer, naam, postcode, telefoonnummer, userID],
                timeout: timeToWait
            }

            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString());
                } else {
                    res.status(200).json(rows);
                }
            })
        }
    },

    // UC-202 Overzicht van studentenhuizen
    getAllStudentHomes(req, res, next) {
        if (req.query.plaats != undefined && req.query.naam != undefined) {
            this.getStudentHomeByNameAndPlace(req, res);
        } else if (req.query.plaats != undefined && req.query.naam == undefined) {
            this.getStudentHomeByPlace(req, res);
        } else if (req.query.naam  != undefined && req.query.plaats == undefined ) {
            this.getStudentHomeByName(req, res);
        } else {
            if (Object.keys(req.query).length === 0) {
                const query = {
                    sql: "SELECT * FROM studenthome",
                    timeout: timeToWait,
                }
                database.query(query, (error, rows, fields) => {
                    if (error) {
                        res.status(500).json(error.toString());
                    } else {
                        res.status(200).json(rows);
                    }
                });
            } else {
                next();
            }
        }
    },

    // UC-203 Details van studentenhuis
    getStudentHomeById(req, res, next) {
        let id = req.params.homeId;
        if (Object.keys(req.query).length === 0) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE ID = ?",
                values: [id],
                timeout: timeToWait,
            }
            database.query(query, (error, rows, fields) => {
                if (rows.length == 0) {
                    res.status(404).send({
                        Message: "Studentenhuis niet gevonden",
                    });
                } else if (error) {
                    res.status(404).send({
                        Message: "Unidentified error: " + error.code,
                    });
                } else {
                    res.status(200).json(rows);
                }
            });
        } else {
            res.status(404).send({
                Message: "Studentenhuis niet gevonden",
            });
        }
    },

    getStudentHomeByNameAndPlace(req, res, next) {
        logger.log("Get studentHomeByNameAndPlace aangeroepen")
        const naam = req.query.naam
        const plaats = req.query.plaats
        const meal = "meal"

        if (plaats == meal) {
            next()
        } else {
            if (naam && plaats) {
                const query = {
                    sql: "SELECT * FROM studenthome WHERE Name = ? AND City = ?",
                    values: [naam, plaats],
                    timeout: timeToWait
                }
                database.query(query, (error, rows, fields) => {
                    if (rows.length === 0) {
                        res.status(404).send({
                            Message: "Naam en plaats studentenhuis niet gevonden",
                        })
                    } else if (error) {
                        res.status(400).send({
                            Message: "Unidentified error: " + error.code
                        })
                    } else {
                        res.status(200).json(rows)
                    }
                })
            } else {
                res.status(404).send({
                    Message: "Naam en plaats studentenhuis niet gevonden",
                })
            }
        }
    },

    getStudentHomeByName(req, res, next) {
        logger.log("Get studentHomeByName aangeroepen")
        const naam = req.query.naam

        if (naam) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE Name = ?",
                values: [naam],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (rows.length === 0) {
                    res.status(404).send({
                        Message: "Naam studentenhuis niet gevonden",
                    })
                } else if (error) {
                    res.status(400).send({
                        Message: "Unidentified error: " + error.code
                    })
                } else {
                    res.status(200).json(rows)
                }
            })
        } else if (plaats) {
            next()
        } else {
            res.status(404).send({
                Message: "Gebruik parameter naam of plaats",
            })
        }
    },

    getStudentHomeByPlace(req, res, next) {
        logger.log("Get studentHomeByPlace aangeroepen")
        const plaats = req.query.plaats

        if (plaats) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE City = ?",
                values: [plaats],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (rows.length == 0) {
                    res.status(404).send({
                        Message: "Plaats studentenhuis niet gevonden",
                    })
                } else if (error) {
                    res.status(400).send({
                        Message: "Unidentified error: " + error.code
                    })
                } else {
                    res.status(200).json(rows)
                }
            })
        } else {
            res.status(404).send({
                Message: "Gebruik parameter naam of plaats",
            })
        }
    },

    // UC-204 Studentenhuis wijzigen
    updateStudentHome(req, res) {
        logger.log("updateStudentHome aangeroepen");
        const homeId = req.params.homeId;
        const naam = req.body.Name;
        const straatnaam = req.body.Address;
        const huisnummer = req.body.House_Nr;
        const postcode = req.body.Postal_Code;
        const plaats = req.body.City;
        const telefoonnummer = req.body.Telephone;
        const userID = req.userId;

        if (userID) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE ID = ? AND UserID = ?",
                values: [homeId, userID],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).send({
                        Message: "ID not found fill in a valid ID"
                    })
                } else if (rows.length != 0) {
                    const query = {
                        sql: "UPDATE studenthome SET Name = ?, Address = ?, House_Nr = ?, Postal_Code = ?, Telephone = ?, City = ? WHERE ID= ? AND UserID = ?",
                        values: [naam, straatnaam, huisnummer, postcode, telefoonnummer, plaats, homeId, userID],
                        timeout: timeToWait
                    }
                    database.query(query, (error, rows, fields) => {
                        if (rows.length == 0) {
                            res.status(401).send({
                                Message: "StudentHome was not found"
                            });
                        } else {
                            res.status(200).send({
                                Message: "ID: " + homeId + " updated"
                            });
                        }
                    });
                } else {
                    res.status(401).send({
                        Message: "You do not have permission to update this studenhome"
                    });
                }
            });
        }
    },
    deleteStudentHome(req, res) {
        logger.log("deleteStudentHome aangeroepen");
        const id = req.params.homeId;
        const userid = req.userId;

        if (id) {
            const query = {
                sql: "SELECT * FROM studenthome WHERE ID = ? AND UserID = ?",
                values: [id, userid],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(404).send({
                        Message: "ID not found fill in a valid ID"
                    })
                } else if (rows.length != 0) {
                    const query = {
                        sql: "DELETE FROM `studenthome` WHERE ID = ? AND UserID = ?",
                        values: [id, userid],
                        timeout: timeToWait
                    }
                    database.query(query, (error, rows, fields) => {
                        if (rows.length == 0) {
                            res.status(401).send({
                                Message: "StudentHome was not found"
                            });
                        } else {
                            res.status(200).send({
                                Message: "ID: " + id + " deleted."
                            });
                        }
                    });
                } else {
                    res.status(401).send({
                        Message: "You do not have permission to delete this studenthome"
                    });
                }
            });
        }
    }
}
