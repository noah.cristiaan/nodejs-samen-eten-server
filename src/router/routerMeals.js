var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
const controllerMeals = require("../controller/controllerMeals");
const authcontroller = require("../controller/authenticationController");
const validationMeals = require("../validation/validationMeals");

// UC-301 Maaltijd aanmaken
router.post('/studenthome/:homeId/meal',
    authcontroller.validateToken,
    controllerMeals.postMeals
);

// UC-302 Maaltijd wijzigen
router.put('/studenthome/:homeId/:mealId',
    authcontroller.validateToken,
    validationMeals.validateNewMeal,
    controllerMeals.updateMeal
);

// UC-303 Lijst van maaltijden opvragen
router.get('/studenthome/:homeId/meal',
    controllerMeals.getAllMeals
);

// UC-304 Details van een maaltijd opvragen
router.get('/studenthome/:homeId/meal/:mealId',
    controllerMeals.getMealById
);

// UC-305 Maaltijd verwijderen
router.delete('/studenthome/:homeId/meal/:mealId',
    authcontroller.validateToken,
    controllerMeals.deleteMeal
);


module.exports = router;