const chai = require("chai")
const timeToWait = 1500
const chaiHttp = require("chai-http")
const server = require("../server")
const database = require("../src/dao/database");
const assert = require("assert")
const logger = require('tracer').console()
const insert = require('./fillTestDatabaseQueries')
const jwt = require('jsonwebtoken')
const { readdirSync } = require("fs");

chai.should();
chai.use(chaiHttp);

describe("Meals", function () {

    // UC-301 Maaltijd aanmaken
    // TC-301-1 Verplicht veld ontbreekt
    // Maaltijd is niet toegevoegdResponsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-301-1 Verplicht veld ontbreekt", (done) => {
        chai.request(server)
            .post("/api/studenthome/1/meal")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .set({
                Allergies: "boter,kaas,eieren",
                Name: "AVG",
                Ingredients: "aardappel,vlees,groente",
                Price: "24,99",
                MaxParticipants: "8"
            })
            .end((err, res) => {
                res.should.have.status(400)
                done()
            })
    })

    // TC-301-2 Niet ingelogd
    // Maaltijd is niet toegevoegdResponsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-301-2 Niet ingelogd", (done) => {
        chai.request(server)
            .post("/api/studenthome/1/meal")
            .send({
                Allergies: "boter,kaas,eieren",
                Name: "AVG",
                Description: "lekker aardappel vlees en groente",
                Ingredients: "aardappel,vlees,groente",
                Price: "24.99",
                MaxParticipants: "8",
            })
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-301-3 Maaltijdsuccesvol toegevoegd
    // Maaltijd bestaat in databaseMaaltijd heeft identificatienr.
    // Responsestatus HTTP code 200 (OK) 
    // Response bevat JSON object metalle gegevens van de maaltijd.

    it("TC-301-3 Maaltijdsuccesvol toegevoegd", (done) => {
        chai.request(server)
            .post("/api/studenthome/1/meal")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Allergies: "boter,kaas,eieren",
                Name: "AVG",
                Description: "lekker aardappel vlees en groente",
                Ingredients: "aardappel,vlees,groente",
                Price: "24.99",
                MaxParticipants: "8",
            })
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-302-1 Verplicht veld ontbreekt
    // Maaltijd is niet gewijzigdResponsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-302-1 Verplicht veld ontbreekt", (done) => {
        chai.request(server)
            .put("/api/studenthome/1/meal/3")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Name: "AVG",
                Description: "lekker aardappel vlees en groente",
                Ingredients: "aardappel,vlees,groente",
                Allergies: "boter,kaas,eieren",
                MaxParticipants: "8",
            })
            .end((err, res) => {
                res.should.have.status(404)
                done()
            })
    })

    // TC-302-2 Niet ingelogd
    // Maaltijd is niet gewijzigdResponsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-302-2 Niet ingelogd", (done) => {
        chai.request(server)
            .put("/api/studenthome/1/1")
            .send({
                Allergies: "boter,kaas,eieren",
                Name: "AVG",
                Description: "lekker aardappel vlees en groente",
                Ingredients: "aardappel,vlees,groente",
                Price: "24,99",
                MaxParticipants: "8",
            })
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-302-3Niet de eigenaar van de data
    // Maaltijd is niet gewijzigdResponsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-302-3 Niet de eigenaar van de data", (done) => {
        chai.request(server)
            .put("/api/studenthome/1/7")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 3 }, 'secret'))
            .send({
                Allergies: "boter,kaas,eieren",
                Name: "AVG",
                Description: "lekker aardappel vlees en groente",
                Ingredients: "aardappel,vlees,groente",
                Price: "24,99",
                MaxParticipants: "8",
            })
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-302-4 Maaltijd bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-302-4 Maaltijd bestaat niet", (done) => {
        chai.request(server)
            .put("/api/studenthome/1/meal/303")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Name: "AVG",
                Description: "lekker aardappel vlees en groente",
                Ingredients: "aardappel,vlees,groente",
                Allergies: "boter,kaas,eieren",
                Price: "24,99",
                MaxParticipants: "8",
            })
            .end((err, res) => {
                res.should.have.status(404)
                done()
            })
    })

    // TC-302-5 Maaltijdsuccesvol gewijzigd
    // Maaltijd is gewijzigd 
    // Responsestatus HTTP code 200 (OK)
    // Response bevat JSON object metalle gegevens van het studentenhuis.

    it("TC-302-5 Maaltijd succesvol gewijzigd", (done) => {
        chai.request(server)
            .put("/api/studenthome/1/1")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Allergies: "boter,kaas,eieren",
                Name: "Sushi",
                Description: "Vis",
                Ingredients: "zalm",
                Price: "24,99",
                MaxParticipants: "8",
            })
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-303-1 Lijst van maaltijden geretourneerd
    // Responsestatus HTTP code 200 (OK)
    // Response bevat JSON object meteen lijst met nul of meer maaltijden.

    it("TC-303-1 Lijst van maaltijden geretourneerd", (done) => {
        chai.request(server)
            .get("/api/studenthome/1/meal")
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-304-1 Maaltijd bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-304-1 Maaltijd bestaat niet", (done) => {
        chai.request(server)
            .get("/api/studenthome/1/meal/666")
            .end((err, res) => {
                res.should.have.status(404)
                done()
            })
    })

    // TC-304-2 Details van maaltijd geretourneerd
    // Responsestatus HTTP code 200 (OK)
    // Response bevat JSON object de details van de gevraagde maaltijd.

    it("TC-304-2 Details van maaltijdgeretourneerd", (done) => {
        chai.request(server)
            .get("/api/studenthome/1/meal/1")
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-305-2 Niet ingelogd
    // Maaltijd is niet verwijderd
    // Responsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-305-2 Niet ingelogd", (done) => {
        chai.request(server)
            .delete("/api/studenthome/1/meal/3")
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-305-3 Niet de eigenaar van de data
    // Maaltijd is niet verwijderd
    // Responsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-305-3 Niet de eigenaar van de data", (done) => {
        chai.request(server)
            .delete("/api/studenthome/1/meal/3")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 3 }, 'secret'))
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-305-4 Maaltijd bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-305-4 Maaltijd bestaat niet", (done) => {
        chai.request(server)
            .delete("/api/studenthome/1/meal/666")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .end((err, res) => {
                res.should.have.status(404)
                done()
            })
    })

    // TC-305-5 Maaltijdsuccesvol verwijderd
    // Maaltijd is verwijderdResponsestatus HTTP code 200 (OK)
    
    it("TC-305-5 Maaltijdsuccesvol verwijderd", (done) => {
        chai.request(server)
            .delete("/api/studenthome/1/meal/3")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })
})



