const logger = require('tracer').console();
const assert = require('assert');

module.exports = {

    validatePostalCodeAndCity(req, res, next) {
        try {
            const id = null;
            const naam = req.body.Name;
            const straatnaam = req.body.Address;
            const huisnummer = req.body.House_Nr;
            const postcode = req.body.Postal_Code;
            const plaats = req.body.City;
            const telefoonnummer = req.body.Telephone;

            assert(typeof naam === "string", "naam is missing");
            assert(typeof straatnaam === "string", "straatnaam is missing");
            assert(typeof huisnummer === "string", "huisnummer is missing");
            assert(typeof postcode === "string", "postcode is missing");
            assert(typeof plaats === "string", "plaats is missing");
            assert(typeof telefoonnummer === "string", "telefoonnummer is missing");

            assert.match(postcode, /[1-9]{1}[0-9]{3}[a-zA-Z]{2}/);
            assert.match(telefoonnummer, /[0]{1}[6]{1}[0-9]{8}/);

            logger.log("PostalCode and City are validated");
            next();
            
        } catch (err) {
            logger.log("data is invalid:", err.message)
            res.status(400).json({
                Message: "Een of meer properties in de request body ontbreken of zijn foutief",
            });
        }
    }
}