const express = require('express');
const logger = require('tracer').console();
const database = require("../dao/database");
const databaseQuerysParticipants = require("../dao/databaseQuerysParticipants");
const timeToWait = 1500;

module.exports = {
    // UC-401 Aanmelden voor maaltijd
    signupForMeal(req, res, next) {
        logger.log('signupForMeal aangeroepen');
        databaseQuerysParticipants.signupForMeal(req, res);
    },

    // UC-402 Afmelden voor maaltijd
    signoffForMeal(req, res) {
        logger.log('signoffForMeal aangeroepen');
        databaseQuerysParticipants.signoffForMeal(req, res);
    },

    // UC-403 Lijst van deelnemers opvragen
    getParticipants(req, res) {
        logger.log('getMealById aangeroepen');
        databaseQuerysParticipants.getParticipants(req, res);
    },

    // UC-404 Details van deelnemer opvragen
    getDetailsParticipants(req, res) {
        logger.log('getDetailsParticipants aangeroepen');
        databaseQuerysParticipants.getDetailsParticipants(req, res);
    }
}