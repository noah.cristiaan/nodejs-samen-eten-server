const database = require("../dao/database");
const logger = require('tracer').console();
const assert = require('assert');
const timeToWait = 1500;

module.exports = {

    validateSignup(req, res, next) {
        let mealID = req.params.mealId;
        let query = {};
        let maxParticipants = {};
        let participants = {};

        query = {
            sql: "SELECT MaxParticipants FROM `meal` WHERE ID = ?",
            values: [mealID],
            timeout: timeToWait
        }
        database.query(query, (error, rows, fields) => {
            if (error) {
                res.status(400).send({
                    Message: error.toString()
                })
            } else if (rows.length == 0) {
                res.status(404).send({
                    Message: "Invalid MealId or HomeId"
                })
            } else {
                maxParticipants = rows[0].MaxParticipants
                query = {
                    sql: "SELECT COUNT(*) AS participants FROM `participants` WHERE MealID = ?",
                    values: [mealID],
                    timeout: timeToWait
                }
                database.query(query, (error, rows, fields) => {
                    if (error) {
                        res.status(400).send({
                            Message: error.toString()
                        })
                    } else if (rows.length == 0) {
                        res.status(404).send({
                            Message: "Invalid MealId"
                        })
                    } else {
                        participants = rows[0].participants
                        if (maxParticipants <= participants) {
                            res.status(401).send({
                                Message: "Meal has reached max nummber of participants"
                            })
                        } else {
                            next()
                        }
                    }
                })
            }
        })
    }
}