const express = require('express');
const logger = require('tracer').console();
const databaseQuerysStudenthome = require("../dao/databaseQuerysStudentHome");

module.exports = {

    // UC-201 Maak studentenhuis
    postNewStudentHomes(req, res) {
        logger.log("Post studentHome aangeroepen");
        databaseQuerysStudenthome.postStudentHome(req, res);
    },

    // UC-202 Overzicht van studentenhuizen
    getStudentHomes(req, res, next) {
        logger.log("Get studentHome aangeroepen");
        databaseQuerysStudenthome.getAllStudentHomes(req, res, next);
    },

    getStudentHomeByID(req, res, next) {
        logger.log("Get studentHomeById aangeroepen");
        databaseQuerysStudenthome.getStudentHomeById(req, res, next);
    },

    getStudentHomeByNameAndPlace(req, res, next) {
        logger.log("Get studentHomeByNameAndPlace aangeroepen");
        databaseQuerysStudenthome.getStudentHomeByNameAndPlace(req, res, next);
    },

    getStudentHomeByName(req, res, next) {
        logger.log("Get studentHomeByName aangeroepen");
        databaseQuerysStudenthome.getStudentHomeByName(req, res, next);
    },

    getStudentHomeByPlace(req, res) {
        logger.log("Get studentHomeByPlace aangeroepen");
        databaseQuerysStudenthome.getStudentHomeByPlace(req, res);
    },

    // UC-204 Studentenhuis wijzigen
    updateStudentHome(req, res) {
        logger.log("Update StudentHome aangeroepen");
        databaseQuerysStudenthome.updateStudentHome(req, res);
    },

    // UC-205 Studentenhuis verwijderen
    deleteStudentHome(req, res) {
        logger.log("Delete StudentHome aangeroepen");
        databaseQuerysStudenthome.deleteStudentHome(req, res);
    },

    // UC-103 Systeeminfo opvragen
    getInfoFunction(req, res) {
        console.log("Info endpoint called")
        let result = {
            message: "Hey, dit is gemaakt door Noah Christiaan de Keijzer",
            studentennummer: 2167874,
            URL : "https://sonarqube.avans-informatica-breda.nl/dashboard?id=helllYeaaaah"
        };
        res.status(200).json(result);
    },
}