const chai = require("chai");
const timeToWait = 1500;
const chaiHttp = require("chai-http");
const server = require("../server");
const database = require("../src/dao/database");
const assert = require("assert");
const logger = require('tracer').console()

const jwt = require('jsonwebtoken')
const { readdirSync } = require("fs");


chai.should();
chai.use(chaiHttp);

describe("studenthomes", function () {

    // TC-201-1 Verplicht veld ontbreekt
    // Studentenhuis is niet toegevoegdResponsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-201-1 Return validation error when values are not present", (done) => {
        chai.request(server)
            .post("/api/studenthomes/")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .query({
                Address: "coolestraat",
                House_Nr: "123",
                Postal_Code: "8247ZS",
                Telephone: "0612367124"
            })
            .end((err, res) => {
                res.should.have.status(400)
                done()
            })
    })

    // TC-201-2 Invalide postcode
    // Studentenhuis is niet toegevoegd
    // Responsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-201-2 Return validation error when postalcode is not the correct value", (done) => {
        chai.request(server)
            .post("/api/studenthomes/")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .query({
                Name: "test",
                Address: "coolestraat",
                House_Nr: "123",
                Postal_Code: "462ZS",
                City: "Heerle",
                Telephone: "0612367124"
            })
            .end((err, res) => {
                res.should.have.status(400)
                done()
            })
    })

    // TC-201-3 Invalide telefoonnummer
    // Studentenhuis is niet toegevoegd
    // Responsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-201-3 Return validation error when phonenumber is not the correct value", (done) => {
        chai.request(server)
            .post("/api/studenthomes/")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Name: "test",
                Address: "coolestraat",
                House_Nr: "123",
                Postal_Code: "4862ZS",
                City: "Heerle",
                Telephone: "212"
            })
            .end((err, res) => {
                res.should.have.status(400)
                done()
            })
    })

    
    // TC-201-4 Studentenhuis bestaat al op dit adres (bestaandpostcode/huisnummer)
    // Studentenhuis is niet toegevoegd
    // Responsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-201-4 Return validation error when input value already exist", (done) => {
        chai.request(server)
            .post("/api/studenthomes/")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 13 }, 'Wachtwoord!'))
            .send({
                Name: "VerwijderHuis",
                Address: "Princenhagen",
                House_Nr: "14",
                Postal_Code: "4225VW",
                City: "Zunderd",
                Telephone: "0638605995"

            })
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-201-5 Niet ingelogd
    // Studentenhuis is niet toegevoegd
    // Responsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie.

    it("TC-201-5 Niet ingelogd", (done) => {
        chai.request(server)
            .post("/api/studenthomes/")
            .send({
                Name: "TestenOsso",
                Address: "Princenhagen",
                House_Nr: "141",
                Postal_Code: "4225VW",
                City: "Zunderd",
                Telephone: "0638605995"

            })
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-201-6 Studentenhuis  succesvol toegevoegd
    // Studentenhuis bestaat in database
    // Studentenhuis heeft identificatienr.
    // Responsestatus HTTP code 200 (OK)
    // Response bevat JSON object metalle gegevens van het studentenhuis.

    it("TC-201-6 Succesfull post request", (done) => {
        chai.request(server)
            .post("/api/studenthomes/")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Name: "Spongebobspineapple",
                Address: "undertheseae",
                House_Nr: "69",
                Postal_Code: "4205XS",
                City: "Bikinibroek",
                Telephone: "0638605995"
            })
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-202-1 Toon nul studentenhuizen
    // Responsestatus HTTP code 200 
    // Response bevat JSON object met lege lijst.

    it("TC-202-1 Toon nul studentenhuizen", (done) => {
        chai.request(server)
            .get("/api/studenthomes/")
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-202-2 Toon twee studentenhuizen
    // Responsestatus HTTP code 200
    // Response bevat JSON object met gegevens van twee studentenhuizen.

    it("TC-202-2 Toon twee studentenhuizen", (done) => {
        chai.request(server)
            .get("/api/studenthomes/")
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-202-3 Toon studentenhuizen met zoekterm op niet-bestaande stad
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.
    
    it("TC-202-3 Toon studentenhuizen met zoekterm op niet-bestaande stad", (done) => {
        chai.request(server)
            .get("/api/studenthomes/")
            .query({
                plaats: "Bikisadasdasdnibroek"
            })
            .end((err, res) => {
                res.should.have.status(404)
                done()
            })
    })

    // TC-202-4 Toon studentenhuizen met zoekterm op niet-bestaande naam
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-202-4 Toon studentenhuizen met zoekterm op niet-bestaande naam", (done) => {
        chai.request(server)
            .get("/api/studenthomes/")
            .query({
                naam: "Spongefdsfbobspineapple"
            })
            .end((err, res) => {
                res.should.have.status(404)
                done()
            })
    })

    // TC-202-5 Toon studentenhuizen met zoekterm op bestaande stad
    // Responsestatus HTTP code 200
    // Response bevat JSON object met gegevens van studentenhuizen.

    it("TC-202-5 Toon studentenhuizen met zoekterm op bestaande stad", (done) => {
        chai.request(server)
            .get("/api/studenthomes/")
            .query({
                plaats: "twente"
            })
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-202-6 Toon studentenhuizen met zoekterm op bestaande naam
    // Responsestatus HTTP code 200
    // Response bevat JSON object met gegevens van studentenhuizen.

    it("TC-202-6 Toon studentenhuizen met zoekterm op bestaande naam", (done) => {
        chai.request(server)
            .get("/api/studenthomes/")
            .query({
                naam: "leukeclownsschool"
            })
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    
    it("TC-202-7 Toon studentenhuizen met zoekterm op bestaande naam en plaats", (done) => {
        chai.request(server)
            .get("/api/studenthomes/")
            .query({
                naam: "leukeclownsschool",
                plaats : "twente"
            })
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-203-1 Studentenhuis-ID bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-203-1 Studentenhuis-ID bestaat niet", (done) => {
        chai.request(server)
            .get("/api/studenthomes/666")
            .end((err, res) => {
                res.should.have.status(404)
                done()
            })
    })

    // TC-203-2 Studentenhuis-ID bestaat
    // Responsestatus HTTP code 200
    // Response bevat JSON object met gegevens van studentenhuis.

    it("TC-203-2 Studentenhuis-ID bestaat", (done) => {
        chai.request(server)
            .get("/api/studenthome/1")
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-204-1 Verplicht veld ontbreekt
    // Responsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-204-1 Verplicht veld ontbreekt", (done) => {
        chai.request(server)
            .put("/api/studenthomes/7")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Name: "Luuksrukbunker",
                Postal_Code: "7346TI",
                Telephone: "0638605995"
            })
            .end((err, res) => {
                res.should.have.status(400)
                done()
            })
    })

    // TC-204-2 Invalide postcode
    // Responsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-204-2 Invalide Postal_Code", (done) => {
        chai.request(server)
            .put("/api/studenthomes/1")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Name: "WesselsRukbunker",
                Address: "baasTweePuntNUl",
                House_Nr: "134",
                Postal_Code: "7s216TIa",
                City: "rotterdam",
                Telephone: "0638605995"
            })
            .end((err, res) => {
                res.should.have.status(400)
                done()
            })
    })

    // TC-204-3 Invalide telefoonnummer
    // Responsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-204-3 Invalide Telephone", (done) => {
        chai.request(server)
            .put("/api/studenthomes/1")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .query({
                Name: "WesselsRukbunker",
                Address: "baasTweePuntNUl",
                House_Nr: "134",
                Postal_Code: "7216TI",
                City: "rotterdam",
                Telephone: "063863s33305995"
            })
            .end((err, res) => {
                res.should.have.status(400)
                done()
            })
    })

    // TC-204-4 Studentenhuis bestaat niet
    // Responsestatus HTTP code 400
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-204-4 Studentenhuis bestaat niet", (done) => {
        chai.request(server)
            .put("/api/studenthomes/303")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Name: "WesselsRukbunker",
                Address: "baasTweePuntNUl",
                House_Nr: "134",
                Postal_Code: "7216TI",
                City: "rotterdam",
                Telephone: "0638605995"
            })
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-204-5 Niet ingelogd
    // Studentenhuis is niet toegevoegd
    // Responsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-204-5 Niet ingelogd", (done) => {
        chai.request(server)
            .put("/api/studenthomes/1")
            .query({
                Name: "WesselsRukbunker",
                Postal_Code: "7216TI",
                Telephone: "0638605995"
            })
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-204-6 Studentenhuis  succesvol gewijzigd
    // Studentenhuis gewijzigd in database
    // Responsestatus HTTP code 200 (OK)
    // Response bevat JSON object metalle gegevens van het studentenhuis

    it("TC-204-6 Studentenhuis  succesvol gewijzigd", (done) => {
        chai.request(server)
            .put("/api/studenthomes/1")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .send({
                Address: "Noahisawesomestraat",
                City: "twente",
                House_Nr: "420",
                Name: "wesselscoolehut",
                Postal_Code: "6341KS",
                Telephone: "0642312814"
            })
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })

    // TC-205-1Studentenhuis bestaat niet
    // Responsestatus HTTP code 404
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-205-1 Studentenhuis bestaat niet", (done) => {
        chai.request(server)
            .delete("/api/studenthomes/307")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 1 }, 'secret'))
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-205-2Niet ingelogd
    // Studentenhuis is niet toegevoegdResponsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-205-2 Niet ingelogd", (done) => {
        chai.request(server)
            .delete("/api/studenthomes/8")
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-205-3 Actor is geen eigenaar
    // Studentenhuis is niet toegevoegdResponsestatus HTTP code 401
    // Response bevat JSON object met daarin generieke foutinformatie, met specifieke foutmelding.

    it("TC-205-3 Actor is geen eigenaar", (done) => {
        chai.request(server)
            .delete("/api/studenthomes/8")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 3 }, 'secret'))
            .end((err, res) => {
                res.should.have.status(401)
                done()
            })
    })

    // TC-205-4Studentenhuis  succesvol verwijderd
    // Studentenhuis verwijderd uit database
    // Responsestatus HTTP code 200 (OK)

    it("TC-205-4 Studentenhuis  succesvol verwijderd", (done) => {
        chai.request(server)
            .delete("/api/studenthomes/6")
            .set('authorization', 'Bearer ' + jwt.sign({ id: 3 }, 'secret'))
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })
})


