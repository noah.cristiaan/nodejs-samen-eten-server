const routes = require('express').Router();
const AuthController = require('../controller/authenticationController');

// UC-102 Login
routes.post('/login',
  AuthController.validateLogin,
  AuthController.login
);

// UC-101 Registreren
routes.post('/register',
  AuthController.validateEmail,
  AuthController.validateRegister,
  AuthController.register
);

routes.get('/validate',
  AuthController.validateToken,
  AuthController.renewToken
);

module.exports = routes;
