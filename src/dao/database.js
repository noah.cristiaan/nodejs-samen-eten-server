const mysql = require('mysql')
const logger = require('tracer').console();
const dbconfig = require('./databaseConfig').dbconfig

const pool = mysql.createPool(dbconfig)

pool.on('connection', function (connection) {
  logger.log('Database connection established')
})

pool.on('acquire', function (connection) {
  logger.log('Database connection aquired')
})

pool.on('release', function (connection) {
  logger.log('Database connection released')
})

module.exports = pool


// let database = {
//   db: [],
//   info: "This is the database",

//   // Gebruik deze add() om vanuit je routes items in de in-memory database te zetten.
//   add(item, callback) {
//     // De setTimeout SIMULEERT in ons geval de vertraging die een echte database
//     // zou hebben. Zodra we dus een echte SQL database gaan gebruiken hebben we de
//     // setTimeout niet meer nodig.
//     setTimeout(() => {
//       // Add id to the item, this is its index in the database.
//       item.id = lastInsertedIndex++;
//       this.db.push(item);
//       // no error occurred
//       callback("success", undefined);
//     }, timeToWait);
//   },

//   getAll(callback) {
//     setTimeout(() => {
//       callback(undefined, this.db);
//     }, timeToWait);
//   },

//   getById(index, callback) {
//     // Gebruik de array.filter() functie hier!
//     // https://developer.mozilla.org/nl/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
//     //
//     const err = { message: "getById not implemented yet", errCode: 501 };
//     callback(err, undefined);
//   },

//   delete(index, callback) {
//     // Gebruik de array.spice() functie om 1 item (op basis van de index!) uit het array te verwijderern
//     // https://developer.mozilla.org/nl/docs/Web/JavaScript/Reference/Global_Objects/Array/splice
//     const err = { message: "delete not implemented yet", errCode: 501 };
//     callback(err, undefined);
//   },

//   getNameAndPlace(itemNaam, itemPlaats, callback) {
//     setTimeout(() => {
//       let itemNotFound = true
//       let gevondenItem = []
//       this.db.forEach(function (item, index, array) {
//         console.log(item, index)
//         if (item.naam.toLocaleLowerCase().includes(itemNaam.toLowerCase()) && item.plaats.toLowerCase().includes(itemPlaats.toLowerCase())) {
//           if (itemNotFound) {
//             itemNotFound = false
//           }
//           gevondenItem.push(item)
//         }
//       })
//       console.log(gevondenItem)

//       if (itemNotFound) {
//         callback(undefined, "error, item niet gevonden")
//       } else {
//         callback(gevondenItem, undefined)
//       }
//     }, timeToWait);
//   },

//   getName(itemNaam, callback) {
//     setTimeout(() => {
//       //get item from array
//       let itemNotFound = true
//       let gevondenItem = []
//       this.db.forEach(function (item, index, array) {
//         console.log(item, index)
//         if (item.naam.toLocaleLowerCase().includes(itemNaam.toLowerCase())) {
//           if (itemNotFound) {
//             itemNotFound = false
//           }
//           gevondenItem.push(item)
//         }
//       })
//       console.log(gevondenItem)

//       if (itemNotFound) {
//         callback(undefined, "error, item niet gevonden")
//       } else {
//         callback(gevondenItem, undefined)
//       }
//     }, timeToWait);
//   },

//   getPlaats(itemPlaats, callback) {
//     setTimeout(() => {
//       //get item from array
//       let itemNotFound = true
//       let gevondenItem = []
//       this.db.forEach(function (item, index, array) {
//         console.log(item, index)
//         if (item.plaats.toLocaleLowerCase().includes(itemPlaats.toLowerCase())) {
//           if (itemNotFound) {
//             itemNotFound = false
//           }
//           gevondenItem.push(item)
//         }
//       })
//       console.log(gevondenItem)

//       if (itemNotFound) {
//         callback(undefined, "error, item niet gevonden")
//       } else {
//         callback(gevondenItem, undefined)
//       }
//     }, timeToWait);
//   }
// }

// module.exports = database;
