const express = require('express');
const logger = require('tracer').console();
const database = require("../dao/database");
const { getAllMeals } = require('../dao/databaseQuerysMeals');
const databaseQuerysMeals = require("../dao/databaseQuerysMeals");
const timeToWait = 1500;

module.exports = {

    // UC-301 Maaltijd aanmaken
    postMeals(req, res){
        logger.log('postMeals aangeroepen');
        databaseQuerysMeals.postMeals(req, res);
    },

    // UC-302 Maaltijd wijzigen
    updateMeal(req, res){
        logger.log('updateMeal aangeroepen');
        databaseQuerysMeals.updateMeal(req, res);
    },

    // UC-303 Lijst van maaltijden opvragen
    getAllMeals(req, res){
        logger.log('getAllMeals aangeroepen');
        databaseQuerysMeals.getAllMeals(req, res);
    },

    // UC-304 Details van een maaltijd opvragen
    getMealById(req, res){
        logger.log('getMealById aangeroepen');
        databaseQuerysMeals.getMealById(req, res);
    },

    // UC-305 Maaltijd verwijderen
    deleteMeal(req, res){
        logger.log('deleteMeal aangeroepen');
        databaseQuerysMeals.deleteMeal(req, res);
    }
}