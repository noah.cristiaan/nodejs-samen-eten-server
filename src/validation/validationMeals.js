const logger = require('tracer').console();
const assert = require('assert');

module.exports = {

    validateNewMeal(req, res, next) {
        logger.log("Validate aangeroepen");

        try {
            let allergies = req.body.Allergies;
            let name = req.body.Name;
            let description = req.body.Description;
            let ingredients = req.body.Ingredients;
            let maxParticipants = req.body.MaxParticipants;
            let price = req.body.Price;

            assert(typeof name === "string", "naam is missing");
            assert(typeof description === "string", "beschrijving is missing");
            assert(typeof ingredients === "string", "ingredienten is missing");
            assert(typeof allergies === "string", "allergien is missing");
            assert(typeof price === "string", "prijs is missing");
            assert(typeof maxParticipants === "string", "aantalpersonen is missing");
            logger.log("validate new meal is accepted");
            next();
            
        } catch (err) {
            logger.log("Invalid data", err.message);
            res.status(400).send({
                Message: "Een of meer properties in de request body ontbreken of zijn foutief",
            })
        }
    }
}