const chai = require("chai");
const timeToWait = 1500;
const chaiHttp = require("chai-http");
const server = require("../server");
const database = require("../src/dao/database");
const assert = require("assert");
const logger = require('tracer').console()
const insert = require('./fillTestDatabaseQueries')
const jwt = require('jsonwebtoken')
const { readdirSync } = require("fs");

chai.should();
chai.use(chaiHttp);

before((done) => {
    database.query(insert.CLEAR_DB, (err, rows, fields) => {
        if (err) {
            logger.log("CLEARING")
            logger.error(`before CLEARING tables: ${err}`)
            done(err)
        } else {
            logger.log("CLEARING")
            done()
        }
    })
})
after((done) => {
    database.query(insert.CLEAR_DB, (err, rows, fields) => {
        if (err) {
            logger.log("CLEARING 2")
            console.log(`after error: ${err}`)
            done(err)
        } else {
            logger.log("CLEARING 2")
            logger.info("After FINISHED")
            done()
        }
    })
})

before((done) => {
    database.query(insert.INSERT_USER, (err, rows, fields) => {
        if (err) {
            logger.error(`Before, insert user query: ${err}`)
            done(err)
        }
        if (rows) {
            done()
        }
    })
})
before((done) => {
    database.query(insert.INSERT_STUDENTHOME, (err, rows, fields) => {
        if (err) {
            logger.error(`Before, insert studenthome query: ${err}`)
            done(err)
        }
        if (rows) {
            done()
        }
    })
})
before((done) => {
    database.query(insert.INSERT_MEAL, (err, rows, fields) => {
        if (err) {
            logger.error(`Before, insert meal query: ${err}`)
            done(err)
        }
        if (rows) {
            done()
        }
    })
})
before((done) => {
    database.query(insert.INSERT_PARTICIPANTS, (err, rows, fields) => {
        if (err) {
            logger.error(`Before, insert meal query: ${err}`)
            done(err)
        }
        if (rows) {
            done()
        }
    })
})

describe("Authenticatie", function () {
    describe("post", function () {
        // TC-101-1
        it("TC-101-1 Verplicht veld ontbreekt", (done) => {
            chai.request(server)
                .post("/api/register/")
                .send({
                    firstname: "luuk",
                    lastname: "bartels1",
                    password: "cooleman34"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        // TC-101-2
        it("TC-101-2 Invalide email adres", (done) => {
            chai.request(server)
                .post("/api/register/")
                .send({
                    firstname: "Tim",
                    lastname: "De Laater",
                    email: "cooleman34cool.com",
                    password: "timmieman12"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        // TC-101-3
        it("TC-101-3 Invalide wachtwoord", (done) => {
            chai.request(server)
                .post("/api/register/")
                .send({
                    firstname: "Tim",
                    lastname: "De Laater",
                    email: "cooleman34@cool.com",
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        // TC-101-4
        it("TC-101-4 Gebruiker bestaat al", (done) => {
            chai.request(server)
                .post("/api/register/")
                .send({
                    email: "mariekevandam@home.nl",
                    firstname: "Marieke",
                    lastname: "van Dam",
                    wachtwoord: "secretsecret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        // TC-101-5
        it("TC-101-5 Gebruiker succesvol geregistreerd", (done) => {
            chai.request(server)
                .post("/api/register/")
                .send({
                    email: "noahdekeijzer@gmail.com",
                    firstname: "Noah",
                    lastname: "de Keijzer",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        
        it("TC-102-1 Verplicht veld ontbreekt", (done) => {
            chai.request(server)
                .post("/api/login/")
                .send({
                    email: "jsmit@server.nl",
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-102-2 Invalide email adres", (done) => {
            chai.request(server)
                .post("/api/login/")
                .set({
                    email: "jsmit@servernl",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-102-3 Invalide wachtwoord", (done) => {
            chai.request(server)
                .post("/api/login/")
                .set({
                    email: "jsmit@server.nl",
                    password: "secert"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-102-4 Gebruiker bestaat niet", (done) => {
            chai.request(server)
                .post("/api/login/")
                .set({
                    email: "wesselKuijstermans@hotmail.com",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-102-5 Gebruiker succesvol ingelogd", (done) => {
            chai.request(server)
                .post("/api/login/")
                .send({
                    email: "jsmit@server.nl",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
})