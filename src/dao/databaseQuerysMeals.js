let timeToWait = 1500;
const { json } = require("body-parser");
const database = require("../dao/database");
const { use } = require("../router/routerStudentHomes");
const { logger } = require("./databaseConfig");

module.exports = {

    // UC-301 Maaltijd aanmaken
    postMeals(req, res) {
        let allergies = req.body.Allergies;
        let name = req.body.Name;
        let description = req.body.Description;
        let ingredients = req.body.Ingredients;
        let maxParticipants = req.body.MaxParticipants;
        let price = req.body.Price;
        let studenthomeID = req.params['homeId'];
        let createdOn = new Date().toLocaleString();
        let offeredOn = new Date().toLocaleString();
        let userID = req.userId;

        if (Object.keys(req.query).length === 0) {
            const query = {
                sql: "INSERT INTO meal (Name, Description, Ingredients, Allergies, CreatedOn, OfferedOn, Price, UserID, StudenthomeID, MaxParticipants) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                values: [name, description, ingredients, allergies, createdOn, offeredOn, price, userID, studenthomeID, maxParticipants],
                timeout: timeToWait
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(400).json(error.toString());
                } else {
                    res.status(200).json(rows);
                }
            })
        }
    },

    // UC-302 Maaltijd wijzigen
    updateMeal(req, res) {
        logger.log("Update meal aangeroepen")
        const id = req.params["homeId"]
        const mealID = req.params["mealId"]
        var date = new Date()
        var month = date.getMonth() + 1
        var day = date.getDate()
        var year = date.getFullYear()

        const naam = req.body.Name;
        const beschrijving = req.body.Description;
        const ingredienten = req.body.Ingredients;
        const allergien = req.body.Allergies;
        const gemaaktOp = year + "-" + month + "-" + day;
        const aangebodenOp = req.body.OfferedOn;
        const prijs = req.body.Price;
        const userid = req.userId;
        const studenthomeId = req.params['homeId'];
        const aantalPersonen = req.body.MaxParticipants;

        if (id && mealID) {
            logger.log(id, mealID);
            const query = {
                sql: "SELECT * FROM meal WHERE ID = ? AND StudenthomeID = ?",
                values: [mealID, id],
                timeout: timeToWait
            }

            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString());
                } if (rows != 0) {
                    logger.log("Has a meal existing");
                    const query = {
                        sql: "UPDATE meal SET Name = ?, Description =?, Ingredients = ? ,Allergies =?, CreatedOn = ?, OfferedOn = ?, Price = ?,UserID = ?,StudenthomeID = ?, MaxParticipants = ?",
                        values: [naam, beschrijving, ingredienten, allergien, gemaaktOp, aangebodenOp, prijs, userid, studenthomeId, aantalPersonen],
                        timeout: timeToWait
                    }
                    database.query(query, (error, rows, fields) => {
                        if (error) {
                            res.status(200).json(error.toString())
                        } else {
                            res.status(200).json("Meal: " + mealID + " Updated succesfuly")
                        }
                    })
                }
                else {
                    res.status(401).json("You do not have permission to update this meal")
                }
            })

        } else {
            res.status(401).send({
                Message: "You do not have permission to update this meal"
            });


        }
    },

    // UC-303 Lijst van maaltijden opvragen
    getAllMeals(req, res) {
        let homeID = req.params['homeId'];

        if (Object.keys(req.query).length === 0) {
            const query = {
                sql: "SELECT * FROM meal WHERE StudenthomeID = ?",
                values: [homeID],
                timeout: timeToWait,
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString());
                } else {
                    res.status(200).json(rows);
                }
            });
        } else {
            next();
        }
    },
    // UC-304 Details van een maaltijd opvragen
    getMealById(req, res, next) {
        let homeID = req.params.homeId;
        let mealID = req.params.mealId;

        if (Object.keys(req.query).length === 0) {
            const query = {
                sql: "SELECT * FROM meal WHERE StudenthomeID = ? AND ID = ?",
                values: [homeID, mealID],
                timeout: timeToWait,
            }
            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(500).json(error.toString());
                } else if (rows.length == 0){
                    res.status(404).json("No meals found");
                } else {
                    res.status(200).json(rows)
                }
            });
        } else {
            next();
        }
    },

    // UC-305 Maaltijd verwijderen
    deleteMeal(req, res) {
        let homeId = req.params.homeId;
        let mealId = req.params.mealId;
        let userId = req.userId;

        if (mealId) {
            const query = {
                sql: "DELETE FROM meal WHERE ID = ? AND UserID = ? AND StudenthomeID = ?",
                values: [mealId, userId, homeId],
                timeout: timeToWait
            }

            database.query(query, (error, rows, fields) => {
                if (error) {
                    res.status(404).send({
                        Message: "ID niet gevonden vul een geldige ID in"
                    });
                } else if (rows.affectedRows == 0) {
                    const query = {
                        sql: "SELECT * FROM meal WHERE ID = ?",
                        values: [mealId],
                        timeout: timeToWait
                    }
                    database.query(query, (error, rows, fields) => {
                        if (rows.length == 0) {
                            res.status(404).send({
                                Message: "Meal not found."
                            });
                        } else {
                            res.status(401).send({
                                Message: "You do not have permission to delete this Meal."
                            });
                        }
                    });
                } else {
                    res.status(200).send({
                        Message: "ID: " + mealId + " verwijderd"
                    });
                }
            });
        }
    },
}
